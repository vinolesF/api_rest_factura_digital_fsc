FACTURANDO ECUADOR API
====================

1) Instalación: 

+ https : git clone https://vinolesF@bitbucket.org/vinolesF/api_rest_factura_digital_fsc.git.
+ shh : git clone git@bitbucket.org:vinolesF/api_rest_factura_digital_fsc.git.

+ Una vez clonado el sistema debe ejecutar un composer install o composer update.
+ Configure su base de datos en su archivo .env.
+ php bin/console doctrine:schema:update --force.
+ composer require encore.
+ yarn install.
+ yarn encore dev ó bien yarn encore production.
+ https://symfony.com/doc/current/frontend/encore/installation.html.
+ https://symfony.com/doc/current/frontend/encore/simple-example.html.

AUTOR: Felipe viñoles <felipe.vinoles@gmail.com>