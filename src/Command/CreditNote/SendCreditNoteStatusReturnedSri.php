<?php

namespace App\Command\CreditNote;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\CreditNoteDocument;
use App\Entity\CreditNote;
use App\Command\CreditNote\CreatePdfCreditNote;

class SendCreditNoteStatusReturnedSri extends ContainerAwareCommand {

    protected function configure() {

        // the name of the command (the part after "bin/console")
        $this->setName('app:send-credit-notes-status-returned-sri')

                // the short description shown while running "php bin/console list"
                ->setDescription('Send CreditNotes Status Returned Sri.');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $response = $this->runCommand();
            if ($response["status"]) {
                $output->writeln($response["message"]);
            } else {
                $output->writeln("Hubo un error al intentar crear los xmls de las notas de credito");
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @return array  boolean true|false
     */
    private function runCommand() {
        try {

            $message = "Documentos xml generados con éxito";
            $status = true;
            $creditNotes = $this->getCreditNotesInStatusReturnedSri();
            if (count($creditNotes) > 0) {
                $this->saveXmlInCreditNote($creditNotes);
            }
        } catch (Exception $ex) {
            $status = false;
            $message = "Hubo un error";
        }
        return ["message" => $message, "status" => $status];
    }

    /**
     * @return array  creditNotes 3  days for renovations
     */
    private function saveXmlInCreditNote($creditNotes) {

        try {
            $em = $this->getContainer()->get('doctrine')->getManager("default");
            foreach ($creditNotes as $creditNote) {
                $document = $em->getRepository('App:CreditNoteDocument')
                        ->findOneBy(array("creditNote" => $creditNote->getId()));
                // create message for creditNote in mail
                if (count($document) == 0) {
                    $docXml = $this->generateXmlDocument($creditNote);
                    $validate = $this->validateVoucherXml($docXml["fileExportContent"], $docXml["passwordAuthorization"], $creditNote);
                    if ($validate) {
                        $document = new CreditNoteDocument();
                        $document->setXmlDocumentLocalName("nota_de_credito_" . $creditNote->getCodeCreditNoteExternal() . ".xml");
                        $document->setCreditNote($creditNote);
                        $creditNote->setStatusSri("approved_sri_xml_local_success");
                        $em->persist($document);
                        $em->flush();
                    }
                } else {
                    $docXml = $this->generateXmlDocument($creditNote);
                    $validate = $this->validateVoucherXml($docXml["fileExportContent"], $docXml["passwordAuthorization"], $creditNote);
                    if ($validate) {
                        $creditNote->setStatusSri("approved_sri_xml_local_success");
                        $document->setXmlDocumentLocalName("nota_de_credito_" . $creditNote->getCodeCreditNoteExternal() . ".xml");
                        $em->flush();
                    }
                }
            }
            echo "\n";
            $status = true;
        } catch (Exception $ex) {
            $status = false;
        }

        return $status;
    }

    private function generateXmlDocument(CreditNote $creditNote) {
        try {
            $passwordAuthorization = $this->generatePasswordAuthorizationSri($creditNote);
            $rootNode = new \SimpleXMLElement("<?xml version='1.0' encoding='UTF-8'?><notaCredito id='comprobante' version='1.0.0'></notaCredito>");
            //Create node two for the document
            $itemOneNode = $rootNode->addChild('infoTributaria');
            $itemOneNode->addChild('ambiente', $creditNote->getCompanyRuc()->getAmbientSri());
            $itemOneNode->addChild('tipoEmision', 1);
            $itemOneNode->addChild('razonSocial', $this->clearSpecialCharacters(strtoupper($creditNote->getCompanyBusinessName()))); // invertir los nombres
            $itemOneNode->addChild('nombreComercial', $this->clearSpecialCharacters(strtoupper($creditNote->getCompanyCommercialName()))); // invertir los nombres
            $itemOneNode->addChild('ruc', trim($creditNote->getCompanyRuc()->getCompanyRuc()));
            $itemOneNode->addChild('claveAcceso', $passwordAuthorization);
            $itemOneNode->addChild('codDoc', '04'); //codigo de documento 01 para notas de credito
            $itemOneNode->addChild('estab', trim($creditNote->getCompanyCodeSri()));
            $itemOneNode->addChild('ptoEmi', trim($creditNote->getCompanyCodeStoreSri()));
            $itemOneNode->addChild('secuencial', trim($creditNote->getSequential()));
            $itemOneNode->addChild('dirMatriz', $this->clearSpecialCharacters($creditNote->getMatrixAddress()));
            //Create node two for the document
            $itemTwoNode = $rootNode->addChild('infoNotaCredito');
            $itemTwoNode->addChild('fechaEmision', $creditNote->getCreateAtReal()->format("d/m/Y"));
            $itemTwoNode->addChild('dirEstablecimiento', $this->clearSpecialCharacters($creditNote->getStoreAddress()));
            if ($creditNote->getSpecialContributorStatus()) {
                $itemTwoNode->addChild('contribuyenteEspecial', trim($creditNote->getSpecialContributor())); //verificar si es o no
            }

            if ($creditNote->getObligedAccountingStatus()) { //verificar si es o no
                $obligedAccounting = "SI";
            } else {
                $obligedAccounting = "NO";
            }

            $itemTwoNode->addChild('tipoIdentificacionComprador', trim($this->clearSpecialCharacters($creditNote->getTypeIdentificationClientSri())));
            $itemTwoNode->addChild('razonSocialComprador', trim($this->clearSpecialCharacters($creditNote->getClientName())));
            $itemTwoNode->addChild('identificacionComprador', trim($creditNote->getIdentificationClient()));
            $itemTwoNode->addChild('obligadoContabilidad', $obligedAccounting);
            $itemTwoNode->addChild('codDocModificado', '01');
            $itemTwoNode->addChild('numDocModificado', $creditNote->getNumberDocomumentoModified());
            $itemTwoNode->addChild('fechaEmisionDocSustento', $creditNote->getBroadcastDateDocomumentoModified()->format("d/m/Y"));
            $itemTwoNode->addChild('totalSinImpuestos', $creditNote->getTotalWithoutTax());
            $itemTwoNode->addChild('valorModificacion', $creditNote->getTotalAmount()); // no estoy seguro
            //create node one for tax
            $itemTwoNodeTax = $itemTwoNode->addChild('totalConImpuestos');
            $itemTwoNodeTaxChild = $itemTwoNodeTax->addChild('totalImpuesto');
            $itemTwoNodeTaxChild->addChild('codigo', 2);
            $itemTwoNodeTaxChild->addChild('codigoPorcentaje', trim($creditNote->getCodeDiscountSri()));
            $itemTwoNodeTaxChild->addChild('baseImponible', $creditNote->getTaxableBase());
            $itemTwoNodeTaxChild->addChild('valor', $creditNote->getValueDiscount());

            $itemTwoNode->addChild('motivo', 'DEVOLUCION');
            //create node details for products
            $itemThreeNode = $rootNode->addChild('detalles');
            if (count($creditNote->getProducts()) > 0) {
                foreach ($creditNote->getProducts() as $product) {
                    $itemThreeNodeChild = $itemThreeNode->addChild('detalle');
                    $descriptionProduct = $this->clearSpecialCharacters($product->getDescription());
                    $descriptionProduct = $this->clearSpecialCharacters($descriptionProduct);
                    $itemThreeNodeChild->addChild('descripcion', $descriptionProduct);
                    $itemThreeNodeChild->addChild('cantidad', trim($product->getQuantity()));
                    $itemThreeNodeChild->addChild('precioUnitario', $product->getPvpUnit());
                    $itemThreeNodeChild->addChild('descuento', $product->getPvpsIndto() - $product->getPvpTotal());
                    $itemThreeNodeChild->addChild('precioTotalSinImpuesto', $product->getPvpTotal());

                    $itemThreeNodeChildTax = $itemThreeNodeChild->addChild('impuestos');
                    $itemThreeNodeChildTaxChild = $itemThreeNodeChildTax->addChild('impuesto');
                    $itemThreeNodeChildTaxChild->addChild('codigo', 2);
                    $itemThreeNodeChildTaxChild->addChild('codigoPorcentaje', 2);
                    $itemThreeNodeChildTaxChild->addChild('tarifa', trim($product->getIva()));
                    //Valor neto x iva entre 100
                    $itemThreeNodeChildTaxChild->addChild('baseImponible', $product->getPvpTotal());
                    $valorTax = ($product->getPvpTotal() * $product->getIva()) / 100;
                    $itemThreeNodeChildTaxChild->addChild('valor', number_format($valorTax, 2, '.', '')); // sacar el precente
                }
            }
            $fileXmlName = dirname(__DIR__) . "/../files/xml_local/credit_note/nota_de_credito_" . $creditNote->getCodeCreditNoteExternal() . ".xml";
            $docXmlExport = $rootNode->asXML($fileXmlName);
            $certPath = dirname(__DIR__) . "/../files/company/key_sri/" . $creditNote->getCompanyRuc()->keyFile->contentUrl; // Convertir pfx to pem 

            $fsignXmlService = $this->getContainer()->get('sign.xml.file');
            $fileSigned = $fsignXmlService->sign($fileXmlName, $certPath, $creditNote->getCompanyRuc()->keyFile->password, "nota_de_credito_" . $creditNote->getCodeCreditNoteExternal() . ".xml", "credit_note");
            $fileExportContent = file_get_contents(dirname(__DIR__) . "/../files/xml_local/credit_note/xsig_nota_de_credito_" . $creditNote->getCodeCreditNoteExternal() . ".xml");
        } catch (\Exception $ex) {
            $em = $this->getContainer()->get('doctrine')->getManager("default");
            $creditNote->setStatusSri("open");
            $creditNote->setMessageErrorLocal("ERROR: " . $ex->getMessage());
            $creditNote->setMessageSri("ERROR: Es posible que la clave de su archivo p12 no sea correcta por favor verifiquela, cambie el nombre del archivo .p12 sin espacios y envielo nuevamente");
            $em->flush();
            return false;
        }
        return ["fileExportContent" => $fileExportContent, "passwordAuthorization" => $passwordAuthorization];
    }

    private function validateVoucherXml($docXml, $password, CreditNote $creditNote) {
        $em = $this->getContainer()->get('doctrine')->getManager("default");
        /* Open Intanciamos el cliente soap para la recepcion de las notas de creditos */
        if ($creditNote->getCompanyRuc()->getAmbientSri() == 1) {
            $wsdl = $this->getContainer()->getParameter('URL_SRI_RECEPTION_TEST');
            $wsdlAuthorization = $this->getContainer()->getParameter('URL_SRI_AUTHORIZATION_TEST');
        } else {
            $wsdl = $this->getContainer()->getParameter('URL_SRI_RECEPTION_PROD');
            $wsdlAuthorization = $this->getContainer()->getParameter('URL_SRI_AUTHORIZATION_PROD');
        }
        /* close Intanciamos el cliente soap para la recepcion de las notas de credito */
        $message = "";
        try {
            $soapClient = new \SoapClient($wsdl);
            $conection = $soapClient->validarComprobante(["xml" => $docXml]);
            $status = false;

            if ($conection->RespuestaRecepcionComprobante->estado == "RECIBIDA") {
                $soapClientAuthorization = new \SoapClient($wsdlAuthorization);
                $authorization = $soapClientAuthorization->autorizacionComprobante(["claveAccesoComprobante" => trim($password)]);

                $statusMessgeSri = $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->estado;
                if ($statusMessgeSri == "AUTORIZADO") {
                    $voucher = $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->comprobante;
                    $docXmlSigneSri = new \SimpleXMLElement($voucher);
                    $fileXmlName = dirname(__DIR__) . "/../files/xml_local/credit_note/aprovada_sri_nota_de_credito_" . $creditNote->getCodeCreditNoteExternal() . ".xml";
                    $docXmlSigneSri->asXML($fileXmlName);
                    $creditNote->setStatusSri("approved_sri");
                    $numberAuthorizationSri = $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->numeroAutorizacion;
                    $creditNote->setNumberAuthorizationSri($numberAuthorizationSri);
                    $authorizationDateStart = $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->fechaAutorizacion;
                    $creditNote->setAuthorizationDateStart(new \DateTime($authorizationDateStart));
                    $creditNote->setMessageSri("NOTA DE CREDITO APROBADA POR EL SRI SIN INCONVENIENTES");
                    $creditNote->setAmbientSri($authorizationDateStart = $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->ambiente);
                    $status = true;
                } else {
                    $message = $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->mensajes->mensaje->mensaje . " - " . $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->mensajes->mensaje->informacionAdicional;
                    $creditNote->setStatusSri("");
                    $em->flush();
                    $creditNote->setStatusSri("returned_sri");
                    $creditNote->setMessageSri("ERROR! DEVUELTA POR: " . $message);
                    $creditNote->setAttempts($creditNote->getAttempts() + 1);
                }
            } else {
                $message = "Imposible capturar el mensaje (el Sri no emitió respuesta)";
                if (isset($conection->RespuestaRecepcionComprobante->comprobantes->comprobante->mensajes->mensaje)) {
                    $message = $conection->RespuestaRecepcionComprobante->comprobantes->comprobante->mensajes->mensaje->mensaje;
                    $idError = $conection->RespuestaRecepcionComprobante->comprobantes->comprobante->mensajes->mensaje->identificador;
                    if (isset($conection->RespuestaRecepcionComprobante->comprobantes->comprobante->mensajes->mensaje->informacionAdicional)) {
                        $message .= "--" . $conection->RespuestaRecepcionComprobante->comprobantes->comprobante->mensajes->mensaje->informacionAdicional;
                    }
                }
                $creditNote->setStatusSri("");
                $em->flush();
                $creditNote->setIdErrorSri($idError);
                $creditNote->setStatusSri("returned_sri");
                $creditNote->setMessageSri("ERROR! DEVUELTA POR: " . $message);
                $creditNote->setAttempts($creditNote->getAttempts() + 1);
                $creditNote->setNumberAuthorizationSri($password);
                if (isset($conection->RespuestaRecepcionComprobante->comprobantes->comprobante->claveAcceso)) {
                    $creditNote->setNumberAuthorizationSri($conection->RespuestaRecepcionComprobante->comprobantes->comprobante->claveAcceso);
                }
            }
        } catch (\Exception $e) {
            $status = false;
            $creditNote->setStatusSri("");
            $em->flush();
            $creditNote->setAttempts($creditNote->getAttempts() + 1);
            $creditNote->setStatusSri("returned_sri");
            $creditNote->setMessageErrorLocal($creditNote->getMessageErrorLocal() . " ----- ERROR -----: " . $e->getMessage());
        }
        echo "...";
        $em->flush();
        return $status;
    }

    /**
     * @return array  creditNotes 3  days for renovations
     */
    private function getCreditNotesInStatusReturnedSri() {

        $em = $this->getContainer()->get('doctrine')->getManager("default");

        $creditNotes = $em->getRepository('App:CreditNote')
                ->findCreditNotesInStatusReturnedSri();
        $listId = [];
        foreach ($creditNotes as $creditNote) {
            $listId[] = $creditNote->getId();
        }
        $em->getRepository('App:CreditNote')
                ->changeStatusInArrayToInProcess($listId);
        return $creditNotes;
    }

    /**
     * @return array  creditNotes 3  days for renovations
     */
    private function generatePasswordAuthorizationSri(CreditNote $creditNote) {
        $sequential = explode("-", $creditNote->getCodeCreditNoteExternal());
        $password = $creditNote->getCreateAtReal()->format("dmY");
        $password .= "04"; //nota de crédito
        $password .= $creditNote->getCompanyRuc()->getCompanyRuc();
        $password .= $creditNote->getCompanyRuc()->getAmbientSri(); //tipo ambiente
        $password .= $sequential[0] . $sequential[1];
        $password .= $creditNote->getSequential();
        $password .= substr($creditNote->getSequential(), 1); // código númerico
        $password .= 1;
        $mod11 = $this->calculateMod11($password);
        $password .= $mod11; // digitador verificador
        return $password;
    }

    /**
     * Extraido de http://ecapy.com/reemplazar-la-n-acentos-espacios-y-caracteres-especiales-con-php-actualizada/
     * Reemplaza todos los acentos por sus equivalentes sin ellos
     *
     * @param $string
     *  string la cadena a sanear
     *
     * @return $string
     *  string saneada
     */
    function clearSpecialCharacters($string) {

        $string = trim($string);

        $string = str_replace(
                ['á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'], ['a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'], $string
        );

        $string = str_replace(
                ['é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'], ['e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'], $string
        );

        $string = str_replace(
                ['í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'], ['i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'], $string
        );

        $string = str_replace(
                ['ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'], ['o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'], $string
        );

        $string = str_replace(
                ['ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'], ['u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'], $string
        );

        $string = str_replace(
                ['ñ', 'Ñ', 'ç', 'Ç'], ['n', 'N', 'c', 'C',], $string);

        $string = str_replace(
                [
            "\\", "¨", "º", "-", "~",
            "#", "@", "|", "!", "\"",
            "·", "$", "%", "&", "/",
            "(", ")", "?", "'", "¡",
            "¿", "[", "^", "<code>", "]",
            "+", "}", "{", "¨", "´",
            ">", "< ", ";", ",", ":",
            ".", " ",
                ], ' ', $string);
        $string = trim(preg_replace('/\s+/', ' ', $string));
        return trim($string);
        }

        /*
         * Calculate mod 11
         */

        private function calculateMod11($data): int {
        $weights = [
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7
        ];
        // If data is not a string...
        if (!is_string($data)) {
            throw new \InvalidArgumentException('debes pasar un argumento de tipo entero.');
        }
        // Split the string into individual characters
        $characters = strrev($data);
        $checkDigitSum = 0;
        for ($i = 0; $i < count($weights); $i++) {
            // Add the multiplication of these two to the checkDigitSum           
            $checkDigitSum += ((int) $weights[$i] * (int) $characters[$i]);
        }
        // Divide the sum by 11 and get the remainder
        $checkDigitRemainder = $checkDigitSum % 11;
        // Minus the remainder from 11
        $checkDigit = 11 - $checkDigitRemainder;
        // Adjust the final values if necessary
        if ($checkDigit === 11) {
            $checkDigit = 0;
        } elseif ($checkDigit === 10) {
            $checkDigit = 1;
        }
        return $checkDigit;
    }

}
