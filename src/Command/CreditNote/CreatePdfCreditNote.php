<?php

namespace App\Command\CreditNote;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Desperado\XmlBundle\Model\XmlPrepare;
use Desperado\XmlBundle\Model\XmlGenerator;
use App\Entity\CreditNoteDocument;
use App\Entity\CreditNote;
use App\Entity\User;

class CreatePdfCreditNote extends ContainerAwareCommand {

    protected function configure() {

        // the name of the command (the part after "bin/console")
        $this->setName('app:create-pdf-credit-note')

                // the short description shown while running "php bin/console list"
                ->setDescription('Send Inovices.');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $response = $this->runCommand();
            if ($response["status"]) {
                $output->writeln($response["message"]);
                //llamar comando que crea los emails una vez genrado los pdf
                $command = $this->getApplication()->find('app:create-emails-for-credit-note');
                $command->run($input, $output);
            } else {
                $output->writeln("Hubo un error al intentar crear los pdfs de las facturas");
            }
        } catch (Exception $ex) {
            echo  $ex->getMessage();
            return false;
        }
    }

    /**
     * @return array  boolean true|false
     */
    private function runCommand() {
        try {

            $message = "Documentos pdfs generados con éxito ";
            $status = true;
            $creditNotes = $this->getCreditNotesInStatusApprovedSri();
            if (count($creditNotes) > 0) {
                $this->savePdfInCreditNote($creditNotes);
            }
        } catch (Exception $ex) {
            $status = false;
            $message = "Hubo un error";
            echo  $ex->getMessage();
        }
        echo "\n";
        return ["message" => $message, "status" => $status];
    }

    /**
     * @return array  creditNotes 3  days for renovations
     */
    private function savePdfInCreditNote($creditNotes) {

        try {
            $em = $this->getContainer()->get('doctrine')->getManager("default");
            foreach ($creditNotes as $creditNote) {
                $document = $em->getRepository('App:CreditNoteDocument')
                        ->findOneBy(array("creditNote" => $creditNote->getId()));
                // create message for creditNote in mail

                $docPdf = $this->generateTemplatePdf($creditNote);
                $creditNote->setStatusSri("approved_sri_all_documents");
                $document->setPdfDocumentName("nota_de_credito_" . $creditNote->getCodeCreditNoteExternal() . ".pdf");
            }
            $em->flush();
            $status = true;
        } catch (\Exception $ex) {
            $status = false;
            echo  $ex->getMessage();
        }
        echo "\n";
        return $status;
    }

    private function generateTemplatePdf(CreditNote$creditNote) { 
        try {
            $template = $this->getContainer()->get('twig')
                    ->render('pdf/credit_note_pdf.html.twig', ['creditNote' => $creditNote]);

            $this->getContainer()->get('knp_snappy.pdf')->generateFromHtml(
                    $template, dirname(__DIR__) . "/../files/credit_note_pdf/nota_de_credito_" . $creditNote->getCodeCreditNoteExternal() . ".pdf"
            );
            echo "...";
            $status = true;
        } catch (\Exception $e) {
            echo $e->getMessage();
            $status = false;
        }
        return $status;
    }

    /**
     * @return array  creditNotes 3  days for renovations
     */
    private function getCreditNotesInStatusApprovedSri() {

        $em = $this->getContainer()->get('doctrine')->getManager("default");

        $creditNotes = $em->getRepository('App:CreditNote')
                ->findCreditNotesInStatusApprovedSri();

        return $creditNotes;
    }

}
