<?php

namespace App\Command\CreditNote;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Desperado\XmlBundle\Model\XmlPrepare;
use Desperado\XmlBundle\Model\XmlGenerator;
use App\Entity\CreditNoteDocument;
use App\Entity\CreditNote;

class CreateEmailsForCreditNote extends ContainerAwareCommand {

    protected function configure() {

        // the name of the command (the part after "bin/console")
        $this->setName('app:create-emails-for-credit-note')

                // the short description shown while running "php bin/console list"
                ->setDescription('Create xmls Local for conection to SRI.');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $response = $this->runCommand();
            if ($response["status"]) {
                $output->writeln($response["message"]);
                //llmar comando para el envio de notas de credito por medio de emails una vez creados los correos
                $command = $this->getApplication()->find('app:send-emails-open');
                $command->run($input, $output);
            } else {
                $output->writeln("Hubo un error al intentar crear los correos de las notas de credito");
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @return array  boolean true|false
     */
    private function runCommand() {
        try {
            $status = true;
            $creditNotes = $this->getCreditNotesInStatusApprovedSri();
            if (count($creditNotes) > 0) {
                $this->generateEmails($creditNotes);
                $message = "correos para notas de credito generados con éxito";
            } else {
                $message = "No hay elementos para crear correos";
            }
        } catch (Exception $ex) {
            $status = false;
            $message = "Hubo un error";
        }
        echo "\n";
        return ["message" => $message, "status" => $status];
    }

    /**
     * @return array  creditNotes 3  days for renovations
     */
    private function generateEmails($creditNotes) {

        try {


            $em = $this->getContainer()->get('doctrine')->getManager("default");
            $emailService = $this->getContainer()->get('save.email.spool.sri');
            foreach ($creditNotes as $creditNote) {
                if ($creditNote->getClientEmail() != "") {
                    $email = $creditNote->getClientEmail();


                    $document = $em->getRepository('App:CreditNoteDocument')
                            ->findOneBy(array("creditNote" => $creditNote->getId()));
                    // create message for creditNote in mail
                    $message = $this->getMessageEmail($creditNote);
                    $emailService->saveEmailCreditNote($email, $creditNote->getClientName(), $message, "Notas de crédito " . $creditNote->getCodeCreditNoteExternal() . " aprobada por SRI", $document);
                } else {
                    $creditNote->setStatusSri("approved_sri_true");
                }

                echo "...";
            }
            $em->flush();
            $status = true;
        } catch (Exception $ex) {
            $status = false;
            echo $ex->getMessage();
        }

        return $status;
    }

    /**
     * @return array  creditNotes 3  days for renovations
     */
    private function getCreditNotesInStatusApprovedSri() {

        $em = $this->getContainer()->get('doctrine')->getManager("default");

        $creditNotes = $em->getRepository('App:CreditNote')
                ->findCreditNotesInStatusApprovedSriAllDocuments();

        return $creditNotes;
    }

    /*
     * Message for client subscriptions expired
     */

    private function getMessageEmail(CreditNote $creditNote) {

        $template = $this->getContainer()->get('twig')
                ->render('emails/send_credit_note_for_client.html.twig', array(
            'creditNote' => $creditNote
                )
        );
        return $template;
    }

}
