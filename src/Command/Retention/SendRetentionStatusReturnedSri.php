<?php

namespace App\Command\Retention;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\RetentionDocument;
use App\Entity\RetentionProvider;
use App\Command\Retention\CreatePdfRetention;

class SendRetentionStatusReturnedSri extends ContainerAwareCommand {

    protected function configure() {

        // the name of the command (the part after "bin/console")
        $this->setName('app:send-retentions-status-returned-sri')

                // the short description shown while running "php bin/console list"
                ->setDescription('Send Retentions Status Returned Sri.');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $response = $this->runCommand();
            if ($response["status"]) {
                $output->writeln($response["message"]);
            } else {
                $output->writeln("Hubo un error al intentar crear los xmls de las retenciones");
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @return array  boolean true|false
     */
    private function runCommand() {
        try {

            $message = "Documentos xml generados con éxito";
            $status = true;
            $retentions = $this->getRetentionsInStatusReturnedSri();
            if (count($retentions) > 0) {
                $this->saveXmlInRetention($retentions);
            }
        } catch (Exception $ex) {
            $status = false;
            $message = "Hubo un error";
        }
        return ["message" => $message, "status" => $status];
    }

    /**
     * @return array  retentions 3  days for renovations
     */
    private function saveXmlInRetention($retentions) {

        try {
            $em = $this->getContainer()->get('doctrine')->getManager("default");
            foreach ($retentions as $retention) {
                $document = $em->getRepository('App:RetentionDocument')
                        ->findOneBy(array("retention" => $retention->getId()));
                // create message for retention in mail
                if (count($document) == 0) {
                    $docXml = $this->generateXmlDocument($retention);
                    $validate = $this->validateVoucherXml($docXml["fileExportContent"], $docXml["passwordAuthorization"], $retention);
                    if ($validate) {
                        $document = new RetentionDocument();
                        $document->setXmlDocumentLocal($docXml["fileExportContent"]);
                        $document->setXmlDocumentLocalName("retencion_" . $retention->getCodeRetentionExternal() . ".xml");
                        $document->setRetention($retention);
                        $retention->setStatusSri("approved_sri_xml_local_success");
                        $em->persist($document);
                        $em->flush();
                    }
                } else {
                    $docXml = $this->generateXmlDocument($retention);
                    $validate = $this->validateVoucherXml($docXml["fileExportContent"], $docXml["passwordAuthorization"], $retention);
                    if ($validate) {
                        $retention->setStatusSri("approved_sri_xml_local_success");
                        $document->setXmlDocumentLocal($docXml["fileExportContent"]);
                        $document->setXmlDocumentLocalName("retencion_" . $retention->getCodeRetentionExternal() . ".xml");
                        $em->flush();
                    }
                }
            }
            echo "\n";
            $status = true;
        } catch (Exception $ex) {
            $status = false;
        }

        return $status;
    }

    private function generateXmlDocument(RetentionProvider $retention) {
        try {
            $passwordAuthorization = $this->generatePasswordAuthorizationSri($retention);
            $rootNode = new \SimpleXMLElement("<?xml version='1.0' encoding='UTF-8'?><comprobanteRetencion id='comprobante' version='1.0.0'></comprobanteRetencion>");
            //Create node two for the document
            $itemOneNode = $rootNode->addChild('infoTributaria');
            $itemOneNode->addChild('ambiente', $retention->getCompanyRuc()->getAmbientSri());
            $itemOneNode->addChild('tipoEmision', 1);
            $itemOneNode->addChild('razonSocial', $this->clearSpecialCharacters(strtoupper($retention->getCompanyBusinessName()))); // invertir los nombres
            $itemOneNode->addChild('nombreComercial', $this->clearSpecialCharacters(strtoupper($retention->getCompanyCommercialName()))); // invertir los nombres
            $itemOneNode->addChild('ruc', trim($retention->getCompanyRuc()->getCompanyRuc()));
            $itemOneNode->addChild('claveAcceso', trim($passwordAuthorization));
            $itemOneNode->addChild('codDoc', '07'); //codigo de documento 01 para retenciones
            $itemOneNode->addChild('estab', trim($retention->getCompanyCodeSri()));
            $itemOneNode->addChild('ptoEmi', trim($retention->getCompanyCodeStoreSri()));
            $itemOneNode->addChild('secuencial', trim($retention->getSequential()));
            $itemOneNode->addChild('dirMatriz', $this->clearSpecialCharacters($retention->getMatrixAddress()));
            //Create node two for the document
            $itemTwoNode = $rootNode->addChild('infoCompRetencion');
            $itemTwoNode->addChild('fechaEmision', $retention->getCreateAtReal()->format("d/m/Y"));
            $itemTwoNode->addChild('dirEstablecimiento', $this->clearSpecialCharacters($retention->getStoreAddress()));
            if ($retention->getSpecialContributorStatus()) {
                $itemTwoNode->addChild('contribuyenteEspecial', trim($retention->getSpecialContributor())); //verificar si es o no
            }

            if ($retention->getObligedAccountingStatus()) { //verificar si es o no
                $obligedAccounting = "SI";
            } else {
                $obligedAccounting = "NO";
            }
            $itemTwoNode->addChild('obligadoContabilidad', $obligedAccounting);
            $itemTwoNode->addChild('tipoIdentificacionSujetoRetenido', $retention->getTypeIdentificationProviderSri());
            $itemTwoNode->addChild('razonSocialSujetoRetenido', $this->clearSpecialCharacters($retention->getProviderName()));
            $itemTwoNode->addChild('identificacionSujetoRetenido', trim($retention->getIdentificationProvider()));
            $itemTwoNode->addChild('periodoFiscal', $retention->getCreateAtReal()->format("m/Y"));
            //create node one for tax
            $itemNodeTax = $rootNode->addChild('impuestos');

            if (count($retention->getTaxes()) > 0) {
                foreach ($retention->getTaxes() as $taxe) {
                    $itemNodeTaxChild = $itemNodeTax->addChild('impuesto');
                    $itemNodeTaxChild->addChild('codigo', $taxe->getCodeTypeSri());
                    $itemNodeTaxChild->addChild('codigoRetencion', trim($taxe->getCodeRetention()));
                    $itemNodeTaxChild->addChild('baseImponible', $taxe->getBase());
                    $itemNodeTaxChild->addChild('porcentajeRetener', $taxe->getPercentage());
                    $itemNodeTaxChild->addChild('valorRetenido', $taxe->getTotal());
                    $itemNodeTaxChild->addChild('codDocSustento', "01");
                    $itemNodeTaxChild->addChild('numDocSustento', $retention->getSupportDocumentNumber());
                    $itemNodeTaxChild->addChild('fechaEmisionDocSustento', $retention->getCreateAtReal()->format("d/m/Y"));
                }
            }
            $fileXmlName = dirname(__DIR__) . "/../files/xml_local/retention/retencion_" . $retention->getCodeRetentionExternal() . ".xml";
            $docXmlExport = $rootNode->asXML($fileXmlName);
            $certPath = dirname(__DIR__) . "/../files/company/key_sri/" . $retention->getCompanyRuc()->keyFile->contentUrl; // Convertir pfx to pem 

            $fsignXmlService = $this->getContainer()->get('sign.xml.file');
            $fileSigned = $fsignXmlService->sign($fileXmlName, $certPath, $retention->getCompanyRuc()->keyFile->password, "retencion_" . $retention->getCodeRetentionExternal() . ".xml", "retention");
            $fileExportContent = file_get_contents(dirname(__DIR__) . "/../files/xml_local/retention/xsig_retencion_" . $retention->getCodeRetentionExternal() . ".xml");
        } catch (\Exception $ex) {
            $em = $this->getContainer()->get('doctrine')->getManager("default");
            $retention->setStatusSri("open");
            $retention->setMessageErrorLocal("ERROR: " . $ex->getMessage());
            $retention->setMessageSri("ERROR: Es posible que la clave de su archivo p12 no sea correcta por favor verifiquela, cambie el nombre del archivo .p12 sin espacios y envielo nuevamente");
            $em->flush();
            return false;
        }
        return ["fileExportContent" => $fileExportContent, "passwordAuthorization" => $passwordAuthorization];
    }

    private function validateVoucherXml($docXml, $password, RetentionProvider $retention) {
        $em = $this->getContainer()->get('doctrine')->getManager("default");
        /* Open Intanciamos el cliente soap para la recepcion de las retenciones */
        if ($retention->getCompanyRuc()->getAmbientSri() == 1) {
            $wsdl = $this->getContainer()->getParameter('URL_SRI_RECEPTION_TEST');
            $wsdlAuthorization = $this->getContainer()->getParameter('URL_SRI_AUTHORIZATION_TEST');
        } else {
            $wsdl = $this->getContainer()->getParameter('URL_SRI_RECEPTION_PROD');
            $wsdlAuthorization = $this->getContainer()->getParameter('URL_SRI_AUTHORIZATION_PROD');
        }
        /* close Intanciamos el cliente soap para la recepcion de las retenciones */
        $message = "";
        try {
            $soapClient = new \SoapClient($wsdl);
            $conection = $soapClient->validarComprobante(["xml" => $docXml]);
            $status = false;
            if ($conection->RespuestaRecepcionComprobante->estado == "RECIBIDA") {
                $soapClientAuthorization = new \SoapClient($wsdlAuthorization);
                $authorization = $soapClientAuthorization->autorizacionComprobante(["claveAccesoComprobante" => trim($password)]);
                $statusMessgeSri = $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->estado;
                if ($statusMessgeSri == "AUTORIZADO") {
                    $voucher = $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->comprobante;
                    $docXmlSigneSri = new \SimpleXMLElement($voucher);
                    $fileXmlName = dirname(__DIR__) . "/../files/xml_local/retention/aprovada_sri_retencion_" . $retention->getCodeRetentionExternal() . ".xml";
                    $docXmlSigneSri->asXML($fileXmlName);
                    $retention->setStatusSri("approved_sri");
                    $numberAuthorizationSri = $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->numeroAutorizacion;
                    $retention->setNumberAuthorizationSri($numberAuthorizationSri);
                    $authorizationDateStart = $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->fechaAutorizacion;
                    $retention->setAuthorizationDateStart(new \DateTime($authorizationDateStart));
                    $retention->setMessageSri("RETENCIÓN APROBADA POR EL SRI SIN INCONVENIENTES");
                    $retention->setAmbientSri($authorizationDateStart = $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->ambiente);
                    $status = true;
                } else {
                    $retention->setStatusSri("");
                    $em->flush();
                    $retention->setStatusSri("returned_sri");
                    $message = $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->mensajes->mensaje->mensaje . " - " . $authorization->RespuestaAutorizacionComprobante->autorizaciones->autorizacion->mensajes->mensaje->informacionAdicional;
                    $retention->setMessageSri("ERROR! DEVUELTA POR: " . $message);
                    $retention->setAttempts($retention->getAttempts() + 1);
                }
            } else {
                $message = "Imposible capturar el mensaje (el Sri no emitió respuesta)";
                if (isset($conection->RespuestaRecepcionComprobante->comprobantes->comprobante->mensajes->mensaje)) {
                    $message = $conection->RespuestaRecepcionComprobante->comprobantes->comprobante->mensajes->mensaje->mensaje;
                    $idError = $conection->RespuestaRecepcionComprobante->comprobantes->comprobante->mensajes->mensaje->identificador;
                    if (isset($conection->RespuestaRecepcionComprobante->comprobantes->comprobante->mensajes->mensaje->informacionAdicional)) {
                        $message .= "--" . $conection->RespuestaRecepcionComprobante->comprobantes->comprobante->mensajes->mensaje->informacionAdicional;
                    }
                }
                $retention->setStatusSri("");
                $em->flush();
                $retention->setIdErrorSri($idError);
                $retention->setStatusSri("returned_sri");
                $retention->setMessageSri("ERROR! DEVUELTA POR: " . $message);
                $retention->setAttempts($retention->getAttempts() + 1);
                $retention->setNumberAuthorizationSri($password);
                if (isset($conection->RespuestaRecepcionComprobante->comprobantes->comprobante->claveAcceso)) {
                    $retention->setNumberAuthorizationSri($conection->RespuestaRecepcionComprobante->comprobantes->comprobante->claveAcceso);
                }
            }
        } catch (\Exception $e) {
            $retention->setStatusSri("");
            $em->flush();
            $status = false;
            $retention->setAttempts($retention->getAttempts() + 1);
            $retention->setStatusSri("open");
            $retention->setMessageErrorLocal($retention->getMessageErrorLocal() . " ----- ERROR -----: " . $e->getMessage());
        }
        echo "...";
        $em->flush();
        return $status;
    }

    /**
     * @return array  retentions 3  days for renovations
     */
    private function getRetentionsInStatusReturnedSri() {

        $em = $this->getContainer()->get('doctrine')->getManager("default");

        $retentions = $em->getRepository('App:RetentionProvider')
                ->findRetentionsInStatusReturnedSri();
        $listId = [];
        foreach ($retentions as $retention) {
            $listId[] = $retention->getId();
        }
        return $retentions;
    }

    /**
     * @return array  retentions 3  days for renovations
     */
    private function generatePasswordAuthorizationSri(RetentionProvider $retention) {
        $sequential = explode("-", $retention->getCodeRetentionExternal());
        $password = $retention->getCreateAtReal()->format("dmY");
        $password .= "07"; //retencion
        $password .= $retention->getCompanyRuc()->getCompanyRuc();
        $password .= $retention->getCompanyRuc()->getAmbientSri(); //tipo ambiente
        $password .= $sequential[0] . $sequential[1];
        $password .= $retention->getSequential();
        $password .= substr($retention->getSequential(), 1); // código númerico
        $password .= 1;
        $mod11 = $this->calculateMod11($password);
        $password .= $mod11; // digitador verificador
        return $password;
    }

    /**
     * Extraido de http://ecapy.com/reemplazar-la-n-acentos-espacios-y-caracteres-especiales-con-php-actualizada/
     * Reemplaza todos los acentos por sus equivalentes sin ellos
     *
     * @param $string
     *  string la cadena a sanear
     *
     * @return $string
     *  string saneada
     */
    function clearSpecialCharacters($string) {

        $string = trim($string);

        $string = str_replace(
                ['á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'], ['a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'], $string
        );

        $string = str_replace(
                ['é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'], ['e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'], $string
        );

        $string = str_replace(
                ['í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'], ['i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'], $string
        );

        $string = str_replace(
                ['ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'], ['o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'], $string
        );

        $string = str_replace(
                ['ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'], ['u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'], $string
        );

        $string = str_replace(
                ['ñ', 'Ñ', 'ç', 'Ç'], ['n', 'N', 'c', 'C',], $string);

        $string = str_replace(
                [
            "\\", "¨", "º", "-", "~",
            "#", "@", "|", "!", "\"",
            "·", "$", "%", "&", "/",
            "(", ")", "?", "'", "¡",
            "¿", "[", "^", "<code>", "]",
            "+", "}", "{", "¨", "´",
            ">", "< ", ";", ",", ":",
            ".", " ",
                ], ' ', $string);
        $string = trim(preg_replace('/\s+/', ' ', $string));
        return trim($string);
        }

        /*
         * Calculate mod 11
         */

        private function calculateMod11($data): int {
        $weights = [
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7,
            2, 3, 4, 5, 6, 7
        ];
        // If data is not a string...
        if (!is_string($data)) {
            throw new \InvalidArgumentException('debes pasar un argumento de tipo entero.');
        }
        // Split the string into individual characters
        $characters = strrev($data);
        $checkDigitSum = 0;
        for ($i = 0; $i < count($weights); $i++) {
            // Add the multiplication of these two to the checkDigitSum           
            $checkDigitSum += ((int) $weights[$i] * (int) $characters[$i]);
        }
        // Divide the sum by 11 and get the remainder
        $checkDigitRemainder = $checkDigitSum % 11;
        // Minus the remainder from 11
        $checkDigit = 11 - $checkDigitRemainder;
        // Adjust the final values if necessary
        if ($checkDigit === 11) {
            $checkDigit = 0;
        } elseif ($checkDigit === 10) {
            $checkDigit = 1;
        }
        return $checkDigit;
    }

}
