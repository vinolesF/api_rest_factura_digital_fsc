<?php

namespace App\Command\Retention;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Desperado\XmlBundle\Model\XmlPrepare;
use Desperado\XmlBundle\Model\XmlGenerator;
use App\Entity\RetentionDocument;
use App\Entity\RetentionProvider;

class CreateEmailsForRetention extends ContainerAwareCommand {

    protected function configure() {

        // the name of the command (the part after "bin/console")
        $this->setName('app:create-emails-for-retention')

                // the short description shown while running "php bin/console list"
                ->setDescription('Create xmls Local for conection to SRI.');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $response = $this->runCommand();
            if ($response["status"]) {
                $output->writeln($response["message"]);
                //llmar comando para el envio de retenciones por medio de emails una vez creados los correos
                $command = $this->getApplication()->find('app:send-emails-open');
                $command->run($input, $output);
            } else {
                $output->writeln("Hubo un error al intentar crear los correos para las retenciones");
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @return array  boolean true|false
     */
    private function runCommand() {
        try {
            $status = true;
            $retentions = $this->getRetentionsInStatusApprovedSri();
            if (count($retentions) > 0) {
                $this->generateEmails($retentions);
                $message = "correos para retenciones generados con éxito";
            } else {
                $message = "No hay elementos para crear correos";
            }
        } catch (Exception $ex) {
            $status = false;
            $message = "Hubo un error";
        }
        echo "\n";
        return ["message" => $message, "status" => $status];
    }

    /**
     * @return array  retentions 3  days for renovations
     */
    private function generateEmails($retentions) {

        try {


            $em = $this->getContainer()->get('doctrine')->getManager("default");
            $emailService = $this->getContainer()->get('save.email.spool.sri');
            foreach ($retentions as $retention) {

                if ($retention->getProviderEmail() != "") {
                    $email = $retention->getProviderEmail();

                    $document = $em->getRepository('App:RetentionDocument')
                            ->findOneBy(array("retention" => $retention->getId()));
                    // create message for retention in mail
                    $message = $this->getMessageEmail($retention);
                    $emailService->saveEmailRetention($email, $retention->getProviderName(), $message, "Retencion " . $retention->getCodeRetentionExternal() . " aprobada por SRI", $document);
                } else {
                    $retention->setStatusSri("approved_sri_true");
                }
                echo "...";
            }
            $em->flush();
            $status = true;
        } catch (Exception $ex) {
            $status = false;
        }

        return $status;
    }

    /**
     * @return array  retentions 3  days for renovations
     */
    private function getRetentionsInStatusApprovedSri() {

        $em = $this->getContainer()->get('doctrine')->getManager("default");

        $retentions = $em->getRepository('App:RetentionProvider')
                ->findRetentionsInStatusApprovedSriAllDocuments();

        return $retentions;
    }

    /*
     * Message for client subscriptions expired
     */

    private function getMessageEmail(RetentionProvider $retention) {

        $template = $this->getContainer()->get('twig')
                ->render('emails/send_retention_for_provider.html.twig', array(
            'retention' => $retention
                )
        );
        return $template;
    }

}
