<?php

namespace App\Command\Retention;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Desperado\XmlBundle\Model\XmlPrepare;
use Desperado\XmlBundle\Model\XmlGenerator;
use App\Entity\RetentionDocument;
use App\Entity\RetentionProvider;
use App\Entity\User;

class CreatePdfRetention extends ContainerAwareCommand {

    protected function configure() {

        // the name of the command (the part after "bin/console")
        $this->setName('app:create-pdf-retention')

                // the short description shown while running "php bin/console list"
                ->setDescription('Send Inovices.');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        try {
            $response = $this->runCommand();
            if ($response["status"]) {
                $output->writeln($response["message"]);
                //llamar comando que crea los emails una vez genrado los pdf
                $command = $this->getApplication()->find('app:create-emails-for-retention');
                $command->run($input, $output);
            } else {
                $output->writeln("Hubo un error al intentar crear los pdfs de las facturas");
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @return array  boolean true|false
     */
    private function runCommand() {
        try {

            $message = "Documentos pdfs generados con éxito ";
            $status = true;
            $retentions = $this->getRetentionsInStatusApprovedSri();
            if (count($retentions) > 0) {
                $this->savePdfInRetention($retentions);
            }
        } catch (Exception $ex) {
            $status = false;
            $message = "Hubo un error";
        }
        echo "\n";
        return ["message" => $message, "status" => $status];
    }

    /**
     * @return array  retentions 3  days for renovations
     */
    private function savePdfInRetention($retentions) {

        try {
            $em = $this->getContainer()->get('doctrine')->getManager("default");
            foreach ($retentions as $retention) {
                $document = $em->getRepository('App:RetentionDocument')
                        ->findOneBy(array("retention" => $retention->getId()));
                // create message for retention in mail

                $docPdf = $this->generateTemplatePdf($retention);
                $retention->setStatusSri("approved_sri_all_documents");
                $document->setPdfDocumentName("retencion_" . $retention->getCodeRetentionExternal() . ".pdf");
            }
            $em->flush();
            $status = true;
        } catch (\Exception $ex) {
            $ex->getMessage();
            $status = false;
        }
        echo "\n";
        return $status;
    }

    private function generateTemplatePdf(RetentionProvider $retention) { 
        try {
            $template = $this->getContainer()->get('twig')
                    ->render('pdf/retention_pdf.html.twig', ['retention' => $retention]);

            $this->getContainer()->get('knp_snappy.pdf')->generateFromHtml(
                    $template, dirname(__DIR__) . "/../files/retention_pdf/retencion_" . $retention->getCodeRetentionExternal() . ".pdf"
            );
            echo "...";
            $status = true;
        } catch (\Exception $e) {
            echo $e->getMessage();
            $status = false;
        }
        return $status;
    }

    /**
     * @return array  retentions 3  days for renovations
     */
    private function getRetentionsInStatusApprovedSri() {

        $em = $this->getContainer()->get('doctrine')->getManager("default");

        $retentions = $em->getRepository('App:RetentionProvider')
                ->findRetentionsInStatusApprovedSri();

        return $retentions;
    }

}
