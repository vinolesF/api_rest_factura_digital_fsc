<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Description of SecurityController
 *
 * @author felipe.vinoles@gmail.com
 */
class SecurityController extends Controller {

    /**
     * @Route("/admin_dashboard/login", name="admin_dashboard_login")
     * @Method({"GET", "POST"})
     */
    public function loginAction(Request $request) {
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    Security::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(Security::AUTHENTICATION_ERROR);
            $session->remove(Security::AUTHENTICATION_ERROR);
        }

        return $this->render('easy_admin/security/login.html.twig', array(
                    // last username entered by the user
                    'last_username' => $session->get(Security::LAST_USERNAME),
                    'error' => $error,
        ));
    }

}
