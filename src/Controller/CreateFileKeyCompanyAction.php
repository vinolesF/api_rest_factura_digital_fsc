<?php

// src/Controller/CreateMediaObjectAction.php

namespace App\Controller;

use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use App\Entity\FileKeyCompany;
use App\Form\FileKeyCompanyType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class CreateFileKeyCompanyAction {

    private $validator;
    private $doctrine;
    private $factory;

    public function __construct(RegistryInterface $doctrine, FormFactoryInterface $factory, ValidatorInterface $validator) {
        $this->validator = $validator;
        $this->doctrine = $doctrine;
        $this->factory = $factory;
    }

    /**
     * @IsGranted("ROLE_USER")
     */
    public function __invoke(Request $request): FileKeyCompany {
        $mediaFileCompany = new FileKeyCompany();
        $data = $request->request->all();

        $form = $this->factory->create(FileKeyCompanyType::class, $mediaFileCompany);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->doctrine->getManager();
//            $company = 
            $em->persist($mediaFileCompany);
            $mediaFileCompany->contentUrl = trim($mediaFileCompany->contentUrl);
            $em->flush();
            // Prevent the serialization of the file property
            $mediaFileCompany->file = null;

            return $mediaFileCompany;
        }

        // This will be handled by API Platform and returns a validation error.
        throw new ValidationException($this->validator->validate($mediaFileCompany));
    }

}
