<?php

namespace App\Services;

/**
 * Class SignXml
 * @package SignXml
 */
class SignXml {

    /**
     * @var string
     */
    private $executableJar;

    /**
     * @var string
     */
    private $java = "java -jar";

    /**
     * FirmarFacturae constructor.
     */
    function __construct() {
        $this->executableJar = dirname(__DIR__) . "/../bin/FirmaGestor.jar";
    }

    /**
     * @param $xml
     * @param $cert
     * @param $password
     * @return string
     * @throws \Exception
     */
    public function sign($xml, $cert, $password, $fileName , $path) {
        try {
            // Generate the command to sign the XML
            $command = $this->buildCommand($xml, $cert, $password, $fileName, $path);

            // Execute the command
            exec($command, $output, $return_var);

            if ($return_var != 0) {
                throw new \Exception("Error: Could not sign the XML", 1);
            }
            $status = true;
        } catch (\Exception $exc) {
            $status = false;
        }
        return $status;
    }

    /**
     * @param $xml
     * @param $cert
     * @param $password
     * @return string
     */
    private function buildCommand($xml, $cert, $password, $fileName, $path) {
        try {
            $name = dirname(__DIR__) . "/files/xml_local/".$path."/xsig_" . $fileName;
            return escapeshellcmd("{$this->java} {$this->executableJar} {$xml} {$name} {$cert} {$password}");
        } catch (\Exception $exc) {
            return false;
        }
    }
}
