<?php

namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
Use App\Entity\EmailSpool;
use App\Entity\InvoiceDocument;
use App\Entity\RetentionDocument;
use App\Entity\CreditNoteDocument;

class SaveEmailSpool {

    private $container;
    private $em;

    public function __construct(EntityManagerInterface $em = null, ContainerInterface $container = null) {
        $this->container = $container;
        $this->em = $em;
    }

    public function saveEmailInvoice($toEmail, $toName = null, $message, $subject, InvoiceDocument $invoiceDocument ) {
        try {
            $em = $this->container->get('doctrine')->getManager("default");
            $email = $em->getRepository('App:EmailSpool')
                    ->findOneBy(array("invoiceDocument" => $invoiceDocument->getId() ));
            
            if (count($email) == 0) {
                $email = new EmailSpool();
                if (!is_null($toName)) {
                    $email->setToName($toName);
                }
                $email->setInvoiceDocument($invoiceDocument);
                $email->setToEmail($toEmail);
                $email->setStatus(0);
                $email->setMessage($message);
                $email->setSubject($subject);
                $email->setAttempts(0);
                $em->persist($email);
            } else {
                $email->setToEmail($toEmail);
                $email->setStatus(0);
                $email->setMessage($message);
                $email->setSubject($subject);
                $email->setAttempts(0);
            }
            $em->flush();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function saveEmailRetention($toEmail, $toName = null, $message, $subject, RetentionDocument $retentionDocument) {

        try {
            $em = $this->container->get('doctrine')->getManager("default");
            $email = $em->getRepository('App:EmailSpool')
                    ->findOneBy(array("retentionDocument" => $retentionDocument->getId()));
            if (count($email) == 0) {
                $email = new EmailSpool();
                if (!is_null($toName)) {
                    $email->setToName($toName);
                }
                $email->setRetentionDocument($retentionDocument);
                $email->setToEmail($toEmail);
                $email->setStatus(0);
                $email->setMessage($message);
                $email->setSubject($subject);
                $email->setAttempts(0);
                $em->persist($email);
            } else {
                $email->setToEmail($toEmail);
                $email->setStatus(0);
                $email->setMessage($message);
                $email->setSubject($subject);
                $email->setAttempts(0);
            }
            $em->flush();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function saveEmailCreditNote($toEmail, $toName = null, $message, $subject, CreditNoteDocument $creditNoteDocument) {

        try {
            $em = $this->container->get('doctrine')->getManager("default");
            $email = $em->getRepository('App:EmailSpool')
                    ->findOneBy(array("creditNoteDocument" => $creditNoteDocument->getId()));
            if (count($email) == 0) {
                $email = new EmailSpool();
                if (!is_null($toName)) {
                    $email->setToName($toName);
                }
                $email->setCreditNoteDocument($creditNoteDocument);
                $email->setToEmail($toEmail);
                $email->setStatus(0);
                $email->setMessage($message);
                $email->setSubject($subject);
                $email->setAttempts(0);
                $em->persist($email);
            } else {
                $email->setToEmail($toEmail);
                $email->setStatus(0);
                $email->setMessage($message);
                $email->setSubject($subject);
                $email->setAttempts(0);
            }
            $em->flush();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

}
