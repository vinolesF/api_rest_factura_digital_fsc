<?php

namespace App\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Entity\Invoice;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Psr\Log\LoggerInterface;

class SendEmailNotification {

    private $mailer;
    private $container;
    private $templating;
    private $validator;
    private $logger;

    public function __construct(\Swift_Mailer $mailer, ContainerInterface $container = null, \Twig_Environment $templating, ValidatorInterface $validator, LoggerInterface $logger) {
        $this->mailer = $mailer;
        $this->container = $container;
        $this->templating = $templating;
        $this->validator = $validator;
        $this->logger = $logger;
    }

    public function sendEmailNotificationInvoice(Invoice $invoice) {
        try {
            // Validate email
            $addresses = $invoice->getClientEmail();
            $send = false;

            if (!empty($addresses)) {
                try {
                    $body = $this->templating->render(
                            'emails/send_notification_error_invoice.html.twig', array(
                        'invoice' => $invoice
                            )
                    );
                    $messageEmail = ( new \Swift_Message('Notificación'))
                            ->setSubject("Error en factura " . $invoice->getCodeInvoiceExternal())
                            ->setFrom("no_replay@facturandoecuador.com")
                            ->setTo("jose.ochoa@interactuaclub.com")
                            ->setBody($body, 'text/html');
                    $send = $this->mailer->send($messageEmail) > 0;
                } catch (\Exception $e) {
                    
                }
            }

            if (!$send) {
                if (!isset($sendStacksOfEmails)) {
                    $sendStacksOfEmails = $this->container->get("send.stacks.of.emails");
                }
                //$sendStacksOfEmails->sendEmailError($invoice->getClient()->getEmail(), isset($body) ? $body : null);
            }
        } catch (\Exception $e) {
           echo $e->getMessage();
            $send = false;
        }

        return $send;
    }

}
