<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Invoice;
use App\DataFixtures\CompanyFixtures;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

/**
 * Description of InvoiceFixtures
 *
 * @author vinoles
 */
class InvoiceFixtures {
//extends Fixture implements DependentFixtureInterface {

//    public function load(ObjectManager $manager) {
//       for ($i = 200000; $i <= 201000; $i ++) {
//            $sequential = str_pad($i, 9, '0', STR_PAD_LEFT);
//            $invoice = new Invoice();
//            $invoice->setAmbientSri("Pruebas load");
//            $invoice->setClientEmail("felipe.vinoles@yopmail.com");
//            $invoice->setClientName("Consumidor Final");
//            $invoice->setTypeIdentificationClientSri("07");
//            $invoice->setIdentificationClient("9999999999999");
//            $invoice->setCodeDiscountSri(2);
//            $invoice->setCodeInvoiceExternal("002-002-" . $sequential);
//            $invoice->setCodePercentageSri(2);
//            $invoice->setCompanyBusinessName("EMPRESA UNO EJEMPLO");
//            $invoice->setCompanyCodeSri("002");
//            $invoice->setCompanyCommercialName("EMPRESA");
//            $invoice->setCompanyCodeStoreSri("002");
//            $invoice->setCompanyRuc($this->getReference("company"));
//            $invoice->setCompanyCodeStoreSri("002");
//            $invoice->setCreateAtReal(new \DateTime("now"));
//            $invoice->setGratificationValue(0);
//            $invoice->setMatrixAddress("Avenida amazonas con 6 de diciembre");
//            $invoice->setMessageSri("POR ENVIAR AL SRI");
//            $invoice->setObligedAccountingStatus(true);
//            $invoice->setPaymentMethodSri("01");
//            $invoice->setPhone("0960268836");
//            $invoice->setSequential($sequential);
//            $invoice->setSpecialContributor("54545454");
//            $invoice->setSpecialContributorStatus(true);
//            $invoice->setStatusSri("open");
//            $invoice->setStoreAddress("Inca con yasuni");
//            $invoice->setTaxableBase(12);
//            $invoice->setTotalAmount(13.44);
//            $invoice->setTotalDiscount(0);
//            $invoice->setTotalWithoutTax(12);
//            $invoice->setValueDiscount(1.44);
//            $invoice->setTotalPaymentInForClient(0);
//            $manager->persist($invoice);
//            $this->addReference("invoice_" . $i, $invoice);
//        }
//        $manager->flush();
//    }
//
//    public function getDependencies() {
//        return array(
//            CompanyFixtures::class,
//        );
//    }

}
