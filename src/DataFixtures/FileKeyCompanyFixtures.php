<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\FileKeyCompany;

/**
 * Description of InvoiceFixtures
 *
 * @author vinoles
 */
class FileKeyCompanyFixtures extends Fixture {

    public function load(ObjectManager $manager) {

        $keyFileCompany = new FileKeyCompany();
        $keyFileCompany->contentUrl = "1791847644001_miguel_angel_paucar_nacata.p12";
        $keyFileCompany->password = "Luzvalle1990";
        $manager->persist($keyFileCompany);
        $this->addReference("key_file",$keyFileCompany);
        $manager->flush();
    }
}
