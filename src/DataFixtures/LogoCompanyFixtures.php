<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\FileLogoCompany;

/**
 * Description of InvoiceFixtures
 *
 * @author vinoles
 */
class LogoCompanyFixtures extends Fixture {

    public function load(ObjectManager $manager) {

        $logoCompany = new FileLogoCompany();
        $logoCompany->contentUrl = "1791847644001_logo_interactua.png";

        $manager->persist($logoCompany);
        $this->addReference("logo", $logoCompany);
        $manager->flush();
    }
}
