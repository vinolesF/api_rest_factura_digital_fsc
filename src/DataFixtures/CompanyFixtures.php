<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Company;
use App\DataFixtures\FileKeyCompanyFixtures;
use App\DataFixtures\LogoCompanyFixtures;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
/**
 * Description of InvoiceFixtures
 *
 * @author vinoles
 */
class CompanyFixtures extends Fixture implements DependentFixtureInterface {

    public function load(ObjectManager $manager) {

        $company = new Company();
        $company->setCompanyBusinessName("COOPERATIVA DE AHORRO Y CREDITO");
        $company->setCompanyCodeSri("002");
        $company->setCompanyCommercialName("COOPERATIVA DE AHORRO");
        $company->setCompanyRuc("1791847644001");
        $company->setMatrixAddress("Avenida amazonas y 18 de septiembre");
        $company->setPhone("0960254569");
        $company->logo =  $this->getReference("logo");
        $company->keyFile =  $this->getReference("key_file");
        $manager->persist($company);
        $this->addReference("company",$company);
        $manager->flush();
    }

    public function getDependencies() {
        return array(
            FileKeyCompanyFixtures::class,
            LogoCompanyFixtures::class,
        );
    }

}
